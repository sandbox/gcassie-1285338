<?php
// $Id$

/**
 * @file
 * Global admin form for anonymous_comment_email_verification module
 */

function anonymous_comment_email_verification_admin_form(&$form_state) {
  $form = array();
  
  $form['anonymous_comment_email_verification_email_subject'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('anonymous_comment_email_verification_email_subject', t('Please verify your email for !site')),
    '#title' => t('Anonymous Comment Email Verification Subject'),  
  );
  
  $form['anonymous_comment_email_verification_email_body'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('anonymous_comment_email_verification_email_body', _cav_default_email()),
    '#title' => t('Anonymous Comment Email Verification Body'),
  );
  
  $period = drupal_map_assoc(array(3600, 10800, 21600, 32400, 43200, 86400, 172800, 259200, 604800, 1209600, 2419200, 4838400, 9676800), 'format_interval');
  $form['anonymous_comment_email_verification_timer'] = array(
    '#type' => 'select',
    '#title' => t('Discard unverified anonymous comments older than'),
    '#default_value'   => variable_get('anonymous_comment_email_verification_timer', 259200),
    '#options' => $period,
    '#description' => t('Requires a correctly configured <a href="@cron">cron maintenance task</a>.', array('@cron' => url('admin/reports/status')))
  );
  
  $form['comment_anon_register_prompt'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prompt anonymous commenters to register after verification?'),
    '#default_value' => variable_get('comment_anon_register_prompt', FALSE),
    '#description' => t('If checked, a message will be presented to anonymous users who verify their comment asking them to register, with a link to the registration page.'),
  );
  
  return system_settings_form($form);
}

