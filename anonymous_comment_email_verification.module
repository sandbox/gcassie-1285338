<?php
// $Id$

/**
 * @file
 * Require anonymous commenters to verify their email address.
 *
 * While enabled, comments submitted by anonymous users on selected content 
 * types are held in 
 */

/**
 * Implementation of hook_menu().
 */  

function anonymous_comment_email_verification_menu() {
  $items = array();
  
  // this is the link that gets sent in anon emails
  $items['comment/verify/%/%'] = array(
    'title' => 'Verify signature',
    'page callback' => 'anonymous_comment_email_verification_page',
    'page arguments' => array(2, 3),
    'access arguments' => array('post comments'),
    'type' => MENU_CALLBACK,    
  );
  
  $items['admin/settings/anonymous_comment_email_verification'] = array(
    'title' => 'Anonymous comment verification',
    'page callback' => 'drupal_get_form', 
    'page arguments' => array('anonymous_comment_email_verification_admin_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'anonymous_comment_email_verification.admin.inc',
  );
  
  return $items;
}

/**
 * Verifies a link sent to an email address provided by an anonymous commenter.
 *
 * After verification, if anon users have rights to publish comments without 
 * approval, publish the comment. Otherwise, put it into the moderation queue.
 * The commenter is redirected to the node the comment is on. 
 * 
 * If verification fails, the visitor is redirected to the site homepage.  
 * 
 * If comment_anon_register_prompt is true, the commenter will be shown a link
 * suggesting they register an account. 
 *
 * @param $cavid
 *   DB primary key for the anonymous_comment_email_verification table
 * @param $hash
 *   Hash code generated, stored, and emailed when the anon comment was created  
 */

function anonymous_comment_email_verification_page($cavid, $hash) {
  $edit = db_result(db_query("SELECT edit FROM {anonymous_comment_email_verification} WHERE cavid = %d AND hash = '%s'", $cavid, $hash));
  if ($edit) {
    $edit = unserialize($edit);
    comment_save($edit);
    drupal_set_message(t('Thank you, your email address has been verified'));
    if (variable_get('comment_anon_register_prompt', FALSE)) {
      drupal_set_message(t('Consider <a href="/user/register">joining this site</a> to make commenting easier in the future!'));
    }
    // we're done with this stored comment; clear it out.
    db_query("DELETE FROM {anonymous_comment_email_verification} WHERE cavid = %d AND hash = '%s'", $cavid, $hash);
    drupal_goto('node/'. check_plain($edit['nid']));    
  }
  else {
    drupal_set_message(t('This verification link is either not valid or has expired'), 'warning');
    drupal_goto('');
  }
}

/**
 * Implementation of hook_comment().
 */  

function anonymous_comment_email_verification_comment(&$a1, $op) {
  switch ($op) {
    // validate that the email being used by an anon poster isn't
    // registered to an account already. if it is, flash a message and a link
    // to login/registration page
    case "validate":
      if (!$a1['uid']) {
        $node = node_load($a1['nid']);
        if (_cav_enabled($node->type)) {
          if ($a1['mail']) {
            $taken = db_result(db_query("SELECT COUNT(uid) FROM {users} WHERE LOWER(mail) = '%s'", $a1['mail']));
    
            if ($taken != 0) {
              if (variable_get('comment_form_location_'. $node->type, COMMENT_FORM_SEPARATE_PAGE) == COMMENT_FORM_SEPARATE_PAGE) {
                $destination = 'destination='. drupal_urlencode("comment/reply/$node->nid#comment-form");
              }
              else {
                $destination = 'destination='. drupal_urlencode("node/$node->nid#comment-form");
              }                    
              form_set_error('mail', t('The email you used belongs to a registered user. Would you like to <a href="@login">Login</a> or <a href="@register">Register</a>?', 
                array('@login' => url('user/login', array('query' => $destination)), '@register' => url('user/register', array('query' => $destination)))));
            }    
          }
        }
      }
      break;
    
    // add a 'verified' flag that themes can see, to remove the (not verified)
    // tag on anon comments in theme_username
    // if we're viewing this comment on a node type that requires verification,
    // it must have been verified.
    case "view": 
        if (!$a1->uid) {
          $node = node_load($a1->nid);
          if (_cav_enabled($node->type)) {
            $a1->verified = TRUE;
          }
        }
      break;   
  }
}

/**
 * Implementation of hook_form_alter().
 */  

function anonymous_comment_email_verification_form_alter(&$form, $form_state, $form_id) {
  // for anon users replace the default submit function of the comment form with our own
  if ($form_id == 'comment_form') {
    global $user;
    if (!$user->uid) {
      $node = node_load($form['nid']['#value']);
      if (_cav_enabled($node->type)) {
        $default_submit_index = array_search('comment_form_submit', $form['#submit']);
        $form['#submit'][$default_submit_index] = '_comment_anon_form_submit';
      }
    }
  }
  // for admins, add a checkbox to content type forms
  else if ($form_id == 'node_type_form') {
    $form['comment']['anonymous_comment_email_verification_label'] = array(
      '#prefix' => '<div><strong>',
      '#value' => t('Anonymous comment verification:'),
      '#suffix' => '</strong></div>',
      '#weight' => 39,
    );
    $form['comment']['anonymous_comment_email_verification_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Require email verification of anonymous comments on this content type.'),
      '#default_value' => variable_get('anonymous_comment_email_verification_enabled_'. $form['#node_type']->type, FALSE),
      '#description' => t('Anonymous commenting for this content type must be set to <em>Anonymous posters must leave their contact information</em> for this to have an effect.'),
      '#weight' => 40,
    );
  }
}

/**
 * Implementation of hook_cron().
 * 
 * clean out entries that have been awaiting verification too long
 */  

function anonymous_comment_email_verification_cron() {
  db_query('DELETE FROM {anonymous_comment_email_verification} WHERE timestamp < %d', time() - variable_get('anonymous_comment_email_verification_timer', 259200));  
}

/**
 * Overrides the default comment submission function for anon commenters
 *
 * Stores the whole serialized form submission in the anonymous_comment_email_verification
 * database table until the link sent in the email is clicked 
 * 
 * @param $form
 *   The comment form
 * @param $form_state
 *   The comment form state, including the values that will be stored  
 */

function _comment_anon_form_submit($form, &$form_state) {
  _comment_form_submit($form_state['values']);
  if ($cid = comment_anon_hold($form_state['values'])) {
    $node = node_load($form_state['values']['nid']);
    $page = comment_new_page_count($node->comment_count, 0, $node);
    $form_state['redirect'] = array('node/'. $node->nid, $page);
    return;
  }
}

function comment_anon_hold($edit) {
  $hash = md5($edit['nid'] . $edit['mail'] . drupal_get_private_key());
  db_query("INSERT INTO {anonymous_comment_email_verification} (hash, timestamp, edit) VALUES ('%s', %d, '%s')", $hash, $edit['timestamp'], serialize($edit));
  $edit['cavid'] = db_last_insert_id('anonymous_comment_email_verification', 'cavid');
  $edit['hash'] = $hash;
  drupal_mail('anonymous_comment_email_verification', 'verify', $edit['mail'], language_default(), array('edit' => $edit));
}

/**
 * Implementation of hook_mail().
 */  

function anonymous_comment_email_verification_mail($key, &$message, $params) {
  if ($key == 'verify') {
    $variables = user_mail_tokens($params['account'], $message['language']);
    $variables['!verify_link'] = url('comment/verify/'. $params['edit']['cavid'] .'/'.  $params['edit']['hash'], array('absolute' => TRUE));
    
    $message['subject'] = t(variable_get('anonymous_comment_email_verification_email_subject', 'Please verify your email for !site'), $variables);
    $message['body'] = t(variable_get('anonymous_comment_email_verification_email_body', _cav_default_email()), $variables);
  }
}

function _cav_default_email() {
  return t('!verify_link');
}

/**
 * Checks if anon comment verification is active for a content type.
 * 
 * Anon content verification is active if anon users have rights to post
 * comments, it has been enabled by an admin for this type, and anon commenters
 * are required to leave their contact info. 
 */

function _cav_enabled($type) {
  $anon = user_load(0);
  return (user_access('post comments', $anon) || user_access('post comments without approval', $anon)) && variable_get('anonymous_comment_email_verification_enabled_'. $type, FALSE) && (variable_get('comment_anonymous_'. $type, 0) == COMMENT_ANONYMOUS_MUST_CONTACT);
}